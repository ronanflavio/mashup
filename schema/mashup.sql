/*
Navicat MySQL Data Transfer

Source Server         : 1. Local
Source Server Version : 50615
Source Host           : 127.0.0.1:3306
Source Database       : mashup

Target Server Type    : MYSQL
Target Server Version : 50615
File Encoding         : 65001

Date: 2015-06-09 10:54:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for locais
-- ----------------------------
DROP TABLE IF EXISTS `locais`;
CREATE TABLE `locais` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `usuario_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of locais
-- ----------------------------
INSERT INTO `locais` VALUES ('2', 'Casa do Ronan', 'Rua José Mauro de Vasconcelos, 155 - Independência, Belo Horizonte - MG, Brasil', '-20.0255', '-44.0282', '5', '2015-05-26 18:30:55', '2015-05-26 18:30:55');
INSERT INTO `locais` VALUES ('3', 'Casa da Priscilla', 'Rua Mario Rocha Castro - Santa Helena, Belo Horizonte - MG, 30642-120, Brasil', '-19.9826', '-44.0167', '5', '2015-05-26 18:31:23', '2015-05-26 18:31:23');
INSERT INTO `locais` VALUES ('5', 'Casa do Marcos', 'Rua Wolney Belém Botelho - Jaqueline, Belo Horizonte - MG, Brasil', '-19.8063', '-43.9356', '5', '2015-05-26 18:47:39', '2015-05-26 18:47:39');
INSERT INTO `locais` VALUES ('6', 'Bairro de BH', 'Nova Suíssa, Belo Horizonte - MG, Brasil', '-19.9331', '-43.978', '5', '2015-05-26 18:51:54', '2015-05-26 18:51:54');
INSERT INTO `locais` VALUES ('7', 'PUC Minas Barreiro', 'PUC Minas - Barreiro - Avenida Afonso Vaz de Melo, 1200 - Barreiro, Belo Horizonte - MG, 30640-070, Brasil', '-19.9772', '-44.0255', '5', '2015-05-26 18:52:47', '2015-05-26 18:52:47');
INSERT INTO `locais` VALUES ('8', 'Avenida Amazonas com Silva Lobo', 'Avenida Amazonas, Belo Horizonte - MG, Brasil', '-19.9305', '-43.9721', '5', '2015-06-02 15:55:20', '2015-06-02 15:55:20');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` int(11) DEFAULT NULL,
  `location_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hometown_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hometown_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
