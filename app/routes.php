<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::match('GET', '/', array(
    'uses' => 'UsuariosController@login',
    'as' => 'login'
));

Route::match('GET', 'ajax/login', array(
    'uses' => 'AjaxController@login',
    'as' => 'ajax_login'
));

Route::group(array('before' => 'auth'), function()
{

    Route::match('GET', 'inicio', array(
        'uses' => 'HomeController@index',
        'as' => 'inicio'
    ));

    Route::match('GET', 'logout', array(
        'uses' => 'UsuariosController@logout',
        'as' => 'logout'
    ));

    Route::match('GET', 'locais/mapa', array(
        'uses' => 'LocaisController@adicionar',
        'as' => 'locais.adicionar'
    ));

    Route::match('POST', 'locais/armazenar', array(
        'uses' => 'LocaisController@armazenar',
        'as' => 'locais.armazenar'
    ));

    Route::match('GET', 'locais/editar', array(
        'uses' => 'LocaisController@editar',
        'as' => 'locais.editar'
    ));

    Route::match('PUT', 'locais/atualizar', array(
        'uses' => 'LocaisController@atualizar',
        'as' => 'locais.atualizar'
    ));

    Route::match('DELETE', 'locais/excluir', array(
        'uses' => 'LocaisController@excluir',
        'as' => 'locais.excluir'
    ));

});

