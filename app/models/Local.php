<?php

class Local extends Eloquent
{

    protected $table = 'locais';
    public static $unguarded = true;

    public function usuario()
    {
        // return $this->belongsTo('Usuario', 'usuario_id');
        return $this->belongsTo('Usuario');
    }

}
