@extends('layout.login')

@section('title')
Login
@endsection

@section('content')

<div class="social-auth-links text-center">
    <fb:login-button scope="public_profile,email,user_hometown,user_location" data-size="large" onlogin="checkLoginState()">
    </fb:login-button>
</div><!-- /.social-auth-links -->

<div id="status" data-url-login="{{ URL::route('ajax_login') }}">
</div>

@endsection

@section('css')
@endsection

@section('js')
@endsection
