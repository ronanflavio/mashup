@extends('layout.default')

@section('title')
Cadastro de locais
@endsection

@section('content')

<div class="col-lg-12">
    <div class="form-group">
        <div class="col-lg-9">
            <label>Pesquisa de endereço</label>
            <input type="text" name="local" class="form-control" placeholder="Digite um endereço...">
        </div>
        <div class="col-lg-3">
            <label>&nbsp;</label>
            <button id="pesquisar" class="btn btn-default form-control"><span class="fa fa-search"></span> Pesquisar</buttonS>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-9">
            <label>&nbsp;</label>
            <div id="map-canvas" class="col-lg-12"></div>
        </div>
        <div class="col-lg-3">
            <label>&nbsp;</label>
            <div id="map-results">
                <div class="well">
                    Nenhum resultado a ser exibido.
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-lg-12">
        <p>
            <h3>Catálogo de endereços cadastrados</h3>
        </p>
    </div>
</div>
<br>

<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Título</th>
            <th>Endereço</th>
            <th>Cadastrado por</th>
            <th class="actions-table">Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($locais as $local)
        <tr>
            <td>{{ $local->titulo }}</td>
            <td>{{ $local->endereco }}</td>
            <td>{{ $local->usuario->name}}</td>
            <td>
                <a href="javascript:void(0);" class="btn-group" onClick="viewPlace({{ '\''.$local->latitude .'\',\''. $local->longitude .'\',\''. $local->endereco.'\'' }})"><span class="fa fa-eye"></span></a>
                <a href="#editar-{{ $local->id }}" class="btn-group" data-toggle="modal"><span class="fa fa-pencil"></span></a>
                <a href="#excluir-{{ $local->id }}" class="btn-group" data-toggle="modal"><span class="fa fa-trash"></span></a>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Título</th>
            <th>Endereço</th>
            <th>Cadastrado por</th>
            <th>Ações</th>
        </tr>
    </tfoot>
</table>

<!-- modal adicionar local -->
<div class="modal fade" id="adicionar-local" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(array('route' => 'locais.armazenar', 'method' => 'POST')) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Adicionar novo local</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <label>Título</label>
                        <input type="text" name="modal_titulo" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label>Endereço</label>
                        <input type="endereco" name="modal_endereco" class="form-control">
                    </div>
                </div>
                <input type="hidden" name="latitude" value="">
                <input type="hidden" name="longitude" value="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Adicionar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- end modal -->

<!-- modal excluir/editar local-->
@foreach ($locais as $local)
<div class="modal fade" id="excluir-{{ $local->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Exclusão de local</h4>
            </div>
            <div class="modal-body">
                Deseja realmente excluir o local <strong>{{ $local->titulo }}</strong>?
            </div>
            <div class="modal-footer">
                {{ Form::open(array('route' => 'locais.excluir', 'method' => 'delete')) }}
                <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                <button type="submit" class="btn btn-primary">Sim</button>
                {{ Form::hidden('id', $local->id) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editar-{{ $local->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(array('route' => 'locais.atualizar', 'method' => 'PUT')) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar local</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <label>Título</label>
                        <input type="text" name="modal_titulo" class="form-control" value="{{ $local->titulo }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label>Endereço</label>
                        <input type="endereco" name="modal_endereco" class="form-control" value="{{ $local->endereco }}">
                    </div>
                </div>
                <input type="hidden" name="latitude" value="{{ $local->latitude }}">
                <input type="hidden" name="longitude" value="{{ $local->longitude }}">
                <input type="hidden" name="local_id" value="{{ $local->id }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endforeach
<!-- end modal -->

@endsection

@section('css')
<script src="http://maps.googleapis.com/maps/api/js"></script>
<style type="text/css">
    #map-canvas {
        height: 480px;
    }
</style>
@endsection

@section('js')
<script type="text/javascript">
    window.addMarkerPlace;
    window.viewPlace;
    jQuery(function($){
        var map,
            mapa_div = document.getElementById('map-canvas');
        function initialize() {
            var local = '<?php echo Auth::user()->location_name; ?>',
                $zoom = 11;
            if (local == '' || local == null){
                local = 'brasil';
                $zoom = 4;
            }
            var url = 'http://maps.googleapis.com/maps/api/geocode/json?address='+local+'&sensor=true';
            url = encodeURI(url);

            $.getJSON(url, {}, function(response){
                console.log(url);
                if (response.status != 'ZERO_RESULTS') {
                    var latitude_user = response.results[0].geometry.location.lat;
                        longitude_user = response.results[0].geometry.location.lng;
                    var mapProp = {
                        center:new google.maps.LatLng(latitude_user,longitude_user),
                        zoom: $zoom,
                        mapTypeId:google.maps.MapTypeId.ROADMAP
                    };

                    map = new google.maps.Map(mapa_div, mapProp);
                }
            });

        }

        window.viewPlace = function(ltd, lng, address) {
            $(':input[name=local]').val(address);
            addMarkerPlace(ltd, lng, address);
        }

        window.addMarkerPlace = function(ltd, lng, address) {
            $('#map-results').html('<button class="btn btn-primary col-lg-12" data-toggle="modal" data-target="#adicionar-local"><span class="fa fa-check"></span> Adicionar ao catálogo</button>');
            $(':input[name=modal_endereco]').val(address);
            $(':input[name=latitude]').val(ltd);
            $(':input[name=longitude]').val(lng);
            var myLatlng = new google.maps.LatLng(ltd, lng);
            var mapOptions = {
                zoom: 16,
                center: myLatlng
            }
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            var marker = new google.maps.Marker({
                position: myLatlng,
                title:"Hello World!"
            });
            marker.setMap(map);
        };

        function addMarker () {
            var local = $(':input[name=local]')[0].value,
                url = 'http://maps.googleapis.com/maps/api/geocode/json?address='+local+'&sensor=true';

            url = encodeURI(url);

            $.getJSON(url, {}, function(response){
                if (response.status == 'ZERO_RESULTS') {
                    alert('Endereço não encontrado');
                } else {
                    if (response.results.length > 1) { // várias correspondências
                        var $results = $('<ul />');
                        for (i = 0; i < response.results.length; i++) {
                            var ltd = response.results[i].geometry.location.lat,
                                lng = response.results[i].geometry.location.lng,
                                address = response.results[i].formatted_address;
                            $results.append('<li><a href="javascript:void(0);" onClick="addMarkerPlace(\''+ltd+'\',\''+lng+'\',\''+address+'\');">'+address+'</a></li>');
                        }
                        $('#map-results').html($results);
                    } else {
                        var latitude = response.results[0].geometry.location.lat,
                            longitude = response.results[0].geometry.location.lng;
                            address = response.results[0].formatted_address;
                        addMarkerPlace(latitude, longitude, address);
                    }
                }
            });
        }

        $('#pesquisar').on('click', addMarker);

        google.maps.event.addDomListener(window, 'load', initialize);
    });
</script>
{{-- HTML::script('assets/js/api_maps.js') --}}
@endsection
