@extends('layout.default')

@section('title')
Locais cadastrados
@endsection

@section('content')

<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Título</th>
            <th>Endereço</th>
            <th>Cadastrado por</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($locais as $local)
        <tr>
            <td>{{ $local->titulo }}</td>
            <td>{{ $local->endereco }}</td>
            <td>{{ $local->usuario->name}}</td>
            <td>
                <a href="#" class="btn-group"><span class="fa fa-pencil"></span></a>
                <a href="#excluir-{{ $local->id }}" class="btn-group" data-toggle="modal"><span class="fa fa-trash"></span></a>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Título</th>
            <th>Endereço</th>
            <th>Cadastrado por</th>
            <th>Ações</th>
        </tr>
    </tfoot>
</table>

<!-- modal -->
@foreach ($locais as $local)
<div class="modal fade" id="excluir-{{ $local->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Exclusão de local</h4>
            </div>
            <div class="modal-body">
                Deseja realmente excluir o local <strong>{{ $local->titulo }}</strong>?
            </div>
            <div class="modal-footer">
                {{ Form::open(array('route' => 'locais.excluir', 'method' => 'delete')) }}
                <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                <button type="submit" class="btn btn-primary">Sim</button>
                {{ Form::hidden('id', $local->id) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- end modal -->

@endsection

@section('css')
@endsection

@section('js')
@endsection
