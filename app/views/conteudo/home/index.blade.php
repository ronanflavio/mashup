@extends('layout.default')

@section('title')
Início
@endsection

@section('content')

<div class="well center">
    <h1 class="text-muted">Bem vindo(a) à <em>Agenda Mashup.</em></h1>
    <h3 class="text-muted">Cadastre seus endereços de forma simples! Para começar, clique no botão abaixo!</h3>
    <br>
    <a href="{{ URL::route('locais.adicionar') }}" class="btn btn-lg btn-success"><span class="fa fa-play"></span> Começar agora!</a>
</div>

@endsection

@section('css')
@endsection

@section('js')
@endsection
