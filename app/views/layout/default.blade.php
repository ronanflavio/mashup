<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mashup | @yield('title')</title>

        <!-- metadata -->
        {{ View::make('layout.partials.metadata') }}

        @yield('css')

    </head>
    <body class="skin-blue sidebar-mini">

        <div id="status-logged" data-url-redirect-login="{{ URL::route('login') }}"></div>

        <div class="wrapper">

            {{ View::make('layout.partials.header') }}

            {{ View::make('layout.partials.aside-menu') }}

            <!-- Content Wrapper. Contains page content -->

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        @yield('title')
                    </h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">

                            <div class="box">
                                <div class="box-header">
                                    @if (Session::has('message'))
                                        <div class="row-fluid">
                                            <div class="alert alert{{ Session::has('type') ? ' alert-'.Session::get('type') : '' }} alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                                <h4>
                                                    @if (Session::get('type') == 'success')
                                                        <i class="icon fa fa-check"></i> Sucesso!
                                                    @elseif (Session::get('type') == 'info')
                                                        <i class="icon fa fa-info"></i> Informação.
                                                    @elseif (Session::get('type') == 'warning')
                                                        <i class="icon fa fa-warning"></i> Atenção!
                                                    @elseif (Session::get('type') == 'danger')
                                                        <i class="icon fa fa-ban"></i> Erro!
                                                    @endif
                                                </h4>
                                                {{ Session::get('message') }}
                                                @if ($errors->has())
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="box-body">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Versão</b> 1.0
                </div>
                <strong>PUC Minas Barreiro - Sistemas de Informação.</strong> Todos os direitos reservados.
            </footer>

            {{-- View::make('layout.partials.aside-hide') --}}

        </div><!-- ./wrapper -->

        {{ View::make('layout.partials.bottom-scripts') }}
        <!-- FB Login -->
        {{ HTML::script('assets/js/fb_main.js') }}

        @yield('js')

    </body>
</html>
