<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mashup | @yield('title')</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        {{ HTML::style('vendor/adminlte/bootstrap/css/bootstrap.min.css') }}
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        {{ HTML::style('vendor/adminlte/dist/css/AdminLTE.min.css') }}
        <!-- iCheck -->
        {{ HTML::style('vendor/adminlte/plugins/iCheck/square/blue.css') }}

        {{ HTML::style('assets/css/main.css') }}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ URL::to('/') }}"><b>MASH</b>UP</a>
            </div><!-- /.login-logo -->

            <div class="login-box-body">

                @if (Session::has('message'))
                    <div class="row-fluid">
                        <div class="alert alert{{ Session::has('type') ? ' alert-'.Session::get('type') : '' }} alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <h4>
                                @if (Session::get('type') == 'success')
                                    <i class="icon fa fa-check"></i> Sucesso!
                                @elseif (Session::get('type') == 'info')
                                    <i class="icon fa fa-info"></i> Informação.
                                @elseif (Session::get('type') == 'warning')
                                    <i class="icon fa fa-warning"></i> Atenção!
                                @elseif (Session::get('type') == 'danger')
                                    <i class="icon fa fa-ban"></i> Erro!
                                @endif
                            </h4>
                            {{ Session::get('message') }}
                            @if ($errors->has())
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endif

                <p class="login-box-msg">Faça seu login clicando no botão abaixo</p>

                @yield('content')
            </div>

        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        {{ HTML::script('vendor/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}
        <!-- Bootstrap 3.3.2 JS -->
        {{ HTML::script('vendor/adminlte/bootstrap/js/bootstrap.min.js') }}
        <!-- iCheck -->
        {{ HTML::script('vendor/adminlte/plugins/iCheck/icheck.min.js') }}
        <!-- FB Login -->
        {{ HTML::script('assets/js/fb_main.js') }}
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
