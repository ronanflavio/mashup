<!-- jQuery 2.1.4 -->
{{ HTML::script('vendor/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}
<!-- Bootstrap 3.3.2 JS -->
{{ HTML::script('vendor/adminlte/bootstrap/js/bootstrap.min.js') }}
<!-- DATA TABES SCRIPT -->
{{ HTML::script('vendor/adminlte/plugins/datatables/jquery.dataTables.min.js') }}
{{ HTML::script('vendor/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}
<!-- SlimScroll -->
{{ HTML::script('vendor/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}
<!-- FastClick -->
{{ HTML::script('vendor/adminlte/plugins/fastclick/fastclick.min.js') }}
<!-- AdminLTE App -->
{{ HTML::script('vendor/adminlte/dist/js/app.min.js') }}
<!-- AdminLTE for demo purposes -->
{{ HTML::script('vendor/adminlte/dist/js/demo.js') }}
<!-- page script -->
<script type="text/javascript">
    $(function () {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
