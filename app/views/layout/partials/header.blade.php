<header class="main-header">
    <!-- Logo -->
    <a href="{{ URL::route('inicio') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>M</b>UP</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>MASH</b>UP</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="http://graph.facebook.com/{{ Auth::user()->facebook_id }}/picture" class="user-image" alt="User Image"/>
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="http://graph.facebook.com/{{ Auth::user()->facebook_id }}/picture?type=large" class="img-circle" alt="User Image" />
                            <p>
                                {{ Auth::user()->name }} - Web Developer
                                <small>{{ Auth::user()->email }}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <?php /*
                            <div class="pull-left">
                                <a href="#" class="btn btn-danger btn-flat">Profile</a>
                            </div>
                            */ ?>
                            <div class="pull-right">
                                <a href="{{ URL::route('logout') }}" class="btn btn-danger btn-flat">Sair do sistema</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php /*
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
                */ ?>
            </ul>
        </div>
    </nav>
</header>
