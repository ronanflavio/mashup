<?php

class UsuariosController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function login()
    {
        if (Auth::user())
        {
            return Redirect::route('inicio');
        }
        return View::make('conteudo.autenticacao.index');
    }

    public function logout()
    {
        if (Auth::check())
        {
            Auth::logout();
        }
        Session::flash('type', 'info');
        Session::flash('message', 'Logout realizado com sucesso.');
        return Redirect::route('login');
    }

}
