<?php

class AjaxController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function login()
    {
        $facebook_id = Input::get('facebook_id');
        $usuario = Usuario::where('facebook_id', '=', $facebook_id)->first();
        if (empty($usuario))
        {
            $usuario = Usuario::create(array(
                // basic informations
                'password'     => Hash::make(Input::get('facebook_id')),
                'email'        => Input::get('email'),
                'first_name'   => Input::get('first_name'),
                'gender'       => Input::get('gender'),
                'facebook_id'  => Input::get('facebook_id'),
                'last_name'    => Input::get('last_name'),
                'link'         => Input::get('link'),
                'locale'       => Input::get('locale'),
                'name'         => Input::get('name'),
                'timezone'     => Input::get('timezone'),
                // location informations
                'location_id'  =>Input::get('location_id'),
                'location_name'=>Input::get('location_name'),
                'hometown_id'  =>Input::get('hometown_id'),
                'hometown_name'=>Input::get('hometown_name')
            ));
        }
        $user = array(
            'facebook_id' => $usuario->facebook_id,
            'password' => $usuario->facebook_id
        );
        if (Auth::attempt($user))
        {
            $url = URL::route('inicio');
            Session::flash('type', 'success');
            Session::flash('message', 'Bem vindo '.$usuario->name);
            return Response::json(array(
                'url' => $url
            ));
        }
        $url = URL::route('login');
        Session::flash('type', 'danger');
        Session::flash('message', 'Não foi possível fazer o login.');
        return Response::json(array(
            'url' => $url
        ));
    }

}
