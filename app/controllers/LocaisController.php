<?php

class LocaisController extends BaseController
{

    public function adicionar()
    {
        $locais = Local::all();
        return View::make('conteudo.locais.form')
            ->with('locais', $locais);
    }

    public function armazenar()
    {
        $local = Local::create(array(
            'latitude'   => Input::get('latitude'),
            'longitude'  => Input::get('longitude'),
            'titulo'     => Input::get('modal_titulo'),
            'endereco'   => Input::get('modal_endereco'),
            'usuario_id' => Auth::user()->id
        ));

        return Redirect::route('locais.adicionar')
            ->with('type', 'success')
            ->with('message', 'O local foi salvo com sucesso!');
    }

    public function editar($id)
    {
        $local = Local::find($id);
        return View::make('conteudo.locais.form')
            ->with('local', $local);
    }

    public function atualizar()
    {
        $local = Local::findOrFail(Input::get('local_id'));

        $local->latitude   = Input::get('latitude');
        $local->longitude  = Input::get('longitude');
        $local->titulo     = Input::get('modal_titulo');
        $local->endereco   = Input::get('modal_endereco');
        $local->usuario_id = Auth::user()->id;
        $local->save();

        return Redirect::route('locais.adicionar')
            ->with('type', 'success')
            ->with('message', 'O local foi atualizado com sucesso!');
    }

    public function excluir()
    {
        $id = Input::get('id');
        $local = Local::find($id);
        $local->delete();
        return Redirect::route('locais.adicionar')
            ->with('type', 'success')
            ->with('message', 'O local foi excluiído com êxito.');
    }

}
