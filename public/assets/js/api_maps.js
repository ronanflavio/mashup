var map;

function initialize() {

    var mapProp = {
        center:new google.maps.LatLng(51.508742,-0.120850),
        zoom:1,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map($('.google_map'),mapProp);
}

function addMarker () {
    var local = $('input[name=local]').value,
        url = 'http://maps.googleapis.com/maps/api/geocode/json?address='+local+'&sensor=true';

    url = encodeURI(url);

    $.getJSON(url, {}, function(response){
        if (response.status == 'ZERO_RESULTS') {
            alert('Endereço não encontrado');
        } else {
            var latitude = response.results[0].geometry.location.lat,
                longitude = response.results[0].geometry.location.lng,
                bounds = new google.maps.LatLngBounds(),
                myCenter = new google.maps.LatLng(latitude, longitude),
                marker = new google.maps.Marker({
                position:myCenter,
            });
            bounds.extend(myCenter);
            marker.setMap(map);
        }
    });
}
