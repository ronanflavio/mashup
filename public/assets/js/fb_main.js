function statusChangeCallback(response) {
    if (response.status === 'connected') {
        FB.api('/me', function(resposta){
            console.log(resposta);
            var url = $('#status').data('url-login');
            $.getJSON(url,
                {
                    // basic informations
                    'email':resposta.email,
                    'first_name':resposta.first_name,
                    'gender':resposta.gender,
                    'facebook_id':resposta.id,
                    'last_name':resposta.last_name,
                    'link':resposta.link,
                    'locale':resposta.locale,
                    'name':resposta.name,
                    'timezone':resposta.timezone,
                    // location informations
                    'location_id':(resposta.location ? resposta.location.id : null),
                    'location_name':(resposta.location ? resposta.location.name : null),
                    'hometown_id':(resposta.hometown ? resposta.hometown.id : null),
                    'hometown_name':(resposta.hometown ? resposta.hometown.name : null)
                },
                function(retorno){
                    window.location.href=retorno.url;
                });
        });
    } else if (response.status === 'not_authorized') {
        $('$status').html('Faça o login para acessar o sistema.');
    } else {
        $('$status').html('Faça o login para acessar o sistema.');
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1603670876556747',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.2'
    });
};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
        console.log('Successful login for: ' + response.name);
        document.getElementById('status').innerHTML =
            'Thanks for logging in, ' + response.name + '!';
    });
}
