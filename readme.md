## Agenda Mashup

Trabalho em grupo realizado para a disciplina de Tecnologias Web, no curso de Sistemas de Informação (PUC Minas).

O intuito do trabalho foi mesclar funcionalidades locais com web services e demais funcionalidades remotas.

## Tecnologias utilizadas

* PHP

* Laravel 4.2

* MySQL

* Facebook API

* Google Maps API

* Bootstrap Template

### Integrantes do grupo

* Guilherme Lima

* Ícaro Guerra

* Ronan Flávio

* Vinícius Amaral